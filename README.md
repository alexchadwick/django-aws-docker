# Django webapp in Docker, running in AWS services
Expanded on digitalocean tutorial [How to Build a Django and Gunicorn Application with Docker](https://www.digitalocean.com/community/tutorials/how-to-build-a-django-and-gunicorn-application-with-docker)
Uses:
 - Django
 - Docker
 - Gitlab container registry
 - AWS S3, RDS, EC2, Fargate
## Quick Notes
Environmental variables are stored in .env [(docker-compose docs)](https://docs.docker.com/compose/env-file/), using ./django-aws-test/docker-vars-example as a guide.

## Build and push container image

```sh
$ docker login registry.gitlab.com
$ cd ./django-aws-test
$ docker build -t registry.gitlab.com/alexchadwick/django-aws-docker:<tag> .
$ docker push registry.gitlab.com/alexchadwick/django-aws-docker:<tag>
```